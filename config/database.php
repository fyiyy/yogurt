<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

use yogurt\Env;

return [
    'default' => [
        // 数据库类型：mysql,sqlsrv
        'type'               => 'mysql',
        // 服务器地址
        'hostname'           => Env::get('database.hostname', '127.0.0.1'),
        // 数据库名
        'database'           => Env::get('database.database', 'db1'),
        // 用户名
        'username'           => Env::get('database.username', 'root'),
        // 密码
        'password'           => Env::get('database.password', 'root'),
        // 端口
        'hostport'           => '3306',
        // 连接dsn
        'dsn'                => '',
        // 数据库连接参数
        'params'             => [],
        // 数据库编码默认采用utf8
        'charset'            => 'utf8',
        // 数据库表前缀
        'prefix'             => '',
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'             => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'        => true,
        // 读写分离后 主服务器数量
        'master_number'      => 1
    ],
    'mydatabase' => [
        // 数据库类型：mysql,sqlsrv
        'type'               => 'mysql',
        // 服务器地址
        'hostname'           => Env::get('database.hostname', '192.168.2.8,127.0.0.1,192.168.2.10'),
        // 数据库名
        'database'           => Env::get('database.database', 'my_test'),
        // 用户名
        'username'           => Env::get('database.username', 'root'),
        // 密码
        'password'           => Env::get('database.password', 'root'),
        // 端口
        'hostport'           => '3306',
        // 连接dsn
        'dsn'                => '',
        // 数据库连接参数
        'params'             => [],
        // 数据库编码默认采用utf8
        'charset'            => 'utf8',
        // 数据库表前缀
        'prefix'             => '',
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'             => 1,
        // 数据库读写是否分离 主从式有效
        'rw_separate'        => true,
        // 读写分离后 主服务器数量
        'master_number'      => 1
    ],
    // 更多数据库连接
];
