<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

return [
    // 调试模式
    'debug'                 => false,
    // 访问日志写入
    'log_write'             => true,
    // 默认模板后缀
    'default_template_exit' => 'html',
    // URL模式, 可选值:0=>兼容模式，1=>pathinfo，2=>rewrite
    'url_model'             => 0,
    // 默认模块
    'default_model'         => 'index',
    // 默认控制器
    'default_controller'    => 'index',
    // 默认方法
    'default_action'        => 'index',
    // 模型文件夹,自定义修改必须保证有对应的目录
    'model_directory'       => 'model',
    // 控制器文件夹,自定义修改必须保证有对应的目录
    'controller_directory'  => 'controller',
    // 视图文件夹，自定义修改必须保证有对应的目录
    'view_directory'        => 'view',
    // 程序app文件名
    'app_name'              => 'application',
    // 是否强制开启路由模式
    'url_route_must'        => false,
];
