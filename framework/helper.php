<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------


use yogurt\Builder;
use yogurt\Container;
use yogurt\Exception;

set_error_handler(function ($type, $message, $file, $line) {
    $errorInfo['type'] = $type;
    $errorInfo['message'] = trim($message);
    $errorInfo['file'] = $file;
    $errorInfo['line'] = $line;
    Exception::register($errorInfo);
});

register_shutdown_function(function () {
    Exception::register();
});

if (!function_exists('halt')) {
    function halt($var, $exit = true, $format = true)
    {
        Builder::halt($var, $exit, $format);
    }
}

if (!function_exists('dump')) {
    function dump($var, $exit = true, $format = true)
    {
        Builder::halt($var, $exit, $format);
    }
}

if (!function_exists('request')) {
    function request(): \yogurt\Request
    {
        return yogurt\Request::register();
    }
}

if (!function_exists('response')) {
    function response(): \yogurt\Response
    {
        return yogurt\Response::register();
    }
}

if (!function_exists('json')) {
    function json($data, $die = false)
    {
        return yogurt\Response::json($data, $die);
    }
}

if (!function_exists('input')) {
    function input($key = '')
    {
        return \yogurt\Request::param($key);
    }
}

if (!function_exists('env')) {
    function env($key = '')
    {
        return \yogurt\Env::get($key);
    }
}

if (!function_exists('redirect')) {
    function redirect($url)
    {
        \yogurt\Response::redirect($url);
    }
}

if (!function_exists('url')) {
    function url($model = false)
    {
        return \yogurt\Request::url($model);
    }
}

if (!function_exists('root_path')) {
    function root_path(): string
    {
        return ROOT_PATH;
    }
}

if (!function_exists('app_path')) {
    function app_path(): string
    {
        return APP_PATH;
    }
}

if (!function_exists('runtime_path')) {
    function runtime_path(): string
    {
        return RUNTIME_PATH;
    }
}

if (!function_exists('public_path')) {
    function public_path(): string
    {
        return PUBLIC_PATH;
    }
}

if (!function_exists('config_path')) {
    function config_path(): string
    {
        return CONFIG_PATH;
    }
}

if (!function_exists('app')) {
    function app(): Container
    {
        //实例化容器类
        $container = null;
        if (is_null($container)) {
            $container = new Container();
        }
        return $container;
    }
}


