<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

error_reporting(0);

define('VERSION', 'v1.0.0');
define('ROOT_PATH', __DIR__ . '/../');
define('APP_PATH', ROOT_PATH . 'application/');
define('CONFIG_PATH', ROOT_PATH . 'config/');
define('PUBLIC_PATH', ROOT_PATH . 'public/');
define('RUNTIME_PATH', ROOT_PATH . 'runtime/');
define('TEMP_PATH', RUNTIME_PATH . 'temp/');

// 文件后缀名
define('PHP_SUFFIX', '.php');
define('HTML_SUFFIX', '.html');