<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------
namespace yogurt\validate;


class ValidateRule
{

    /**
     * 默认返回验证提示消息
     * @var string
     */
    public $errorMsg = '';

    /**
     * 默认内置验证提示消息
     * @var string[]
     */
    protected $checkMessage = [
        'equals' => '@:attribute is not equal to @:value',
        'required' => '@:attribute require',
        'min' => '@:attribute cannot be less than @:min',
        'max' => '@:attribute cannot be greater than @:max',
        'number' => '@:attribute must be numeric',
        'email' => '@:attribute must be a mail address',
        'between' => '@:attribute must be between @:begin and @:end',
        'length' => '@:attribute can only be @:length characters long',
    ];

    public function __construct()
    {
    }


    /**
     * 验证是否相等
     * @param $value
     * @return bool
     */
    public function equals($value): bool
    {
        if ($value[1] == $value[2]) {
            return true;
        }
        $this->errorMsg = $value[3][$value[0]] ?? str_replace('@:attribute', $value[0], $this->checkMessage[__FUNCTION__]);
        $this->errorMsg = str_replace('@:value', $value[2], $this->errorMsg);
        return false;
    }


    /**
     * 验证是否为空方法
     * @param $value
     * @return bool
     */
    public function required($value): bool
    {
        if (!empty($value[0]) && !empty($value[1]) && !empty($value[2]) && $value[0] != $value[1]) {
            return true;
        }
        $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', empty($value[1]) ? $value[0] : $value[1], $this->checkMessage[__FUNCTION__]);
        return false;
    }


    /**
     * 验证最小值
     * @param $value
     * @return bool
     */
    public function min($value): bool
    {
        if (!empty($value[0]) && !empty($value[1]) && !empty($value[2]) && $value[0] != $value[1]) {
            if ($value[1] >= $value[2][1]) {
                return true;
            }
        }

        $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', empty($value[0]) ? $value[1] : $value[0], $this->checkMessage[__FUNCTION__]);
        $this->errorMsg = str_replace('@:min', $value[2][1], $this->errorMsg);
        return false;
    }


    /**
     * 验证最大值
     * @param $value
     * @return bool
     */
    public function max($value): bool
    {
        if (!empty($value[0]) && !empty($value[1]) && !empty($value[2]) && $value[0] != $value[1]) {
            if ($value[1] <= $value[2][1]) {
                return true;
            }
        }
        $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', empty($value[0]) ? $value[1] : $value[0], $this->checkMessage[__FUNCTION__]);
        $this->errorMsg = str_replace('@:max', $value[2][1], $this->errorMsg);
        return false;
    }


    /**
     * 验证是否为数字
     * @param $value
     * @return bool
     */
    public function number($value): bool
    {
        if (!is_numeric($value[1])) {
            $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', empty($value[1]) ? $value[0] : $value[1], $this->checkMessage[__FUNCTION__]);
            return false;
        }
        return true;
    }


    /**
     * 验证是否为邮件格式
     * @param $value
     * @return bool
     */
    public function email($value): bool
    {
        if (!filter_var($value[1], FILTER_VALIDATE_EMAIL)) {
            $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', empty($value[1]) ? $value[0] : $value[1], $this->checkMessage[__FUNCTION__]);
            return false;
        }
        return true;
    }


    /**
     * 验证是否在一个范围内
     * @param $value
     * @return bool
     */
    public function between($value): bool
    {
        $range = explode(',', $value[2][1]);
        sort($range);
        if ($value[1] >= $range[0] && $value[1] <= $range[1]) {
            return true;
        }
        $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', $value[0], $this->checkMessage[__FUNCTION__]);
        $this->errorMsg = str_replace(['@:begin', '@:end'], [$range[0], $range[1]], $this->errorMsg);
        return false;
    }


    /**
     * 验证字符串长度，支持长度范围验证
     * @param $value
     * @return bool
     */
    public function length($value): bool
    {
        if (!empty($value[0]) && !empty($value[1]) && !empty($value[2]) && $value[0] != $value[1]) {
            if (is_numeric($value[2][1])) {
                if (strlen($value[1]) == $value[2][1]) {
                    return true;
                }
            }
            if (strpos($value[2][1], ',') !== false) {
                $range = explode(',', $value[2][1]);
                sort($range);
                if (strlen($value[1]) >= $range[0] && strlen($value[1]) <= $range[1]) {
                    return true;
                }
            }

        }
        $this->errorMsg = $value[3][$value[0] . '.' . $value[2][0]] ?? str_replace('@:attribute', empty($value[0]) ? $value[1] : $value[0], $this->checkMessage[__FUNCTION__]);
        $this->errorMsg = str_replace('@:length', $value[2][1], $this->errorMsg);
        return false;
    }


    /**
     * 静态类调用入口
     * @param $method
     * @param $args
     * @return mixed
     */
    public static function __callStatic($method, $args)
    {
        $self = new self();
        return $self->$method($args);
    }


    private function __clone()
    {
    }
}
