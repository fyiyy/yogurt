<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

class Yogurt
{
    /**
     * 当前URL访问的模块
     * @var $module
     */
    public static $module;
    /**
     * 当前URL访问的控制器
     * @var $controller
     */
    public static $controller;
    /**
     * 自动要执行的方法
     * @var $action
     */
    public static $action;

    /**引入文件方法
     * @param $file
     * @return mixed
     */
    public static function includeFile($file)
    {
        return include $file;
    }

    /**
     * 通过配置文件获取默认URL路径
     * @return string
     * @throws Exception
     */
    protected static function getUrlByConfig(): string
    {
        self::$module = empty(Config::get('app.default_model')) ? 'index' : Config::get('app.default_model');
        self::$controller = empty(Config::get('app.default_controller')) ? 'index' : Config::get('app.default_controller');
        self::$action = empty(Config::get('app.default_action')) ? 'index' : Config::get('app.default_action');
        return self::$module . '/' . self::$controller . '/' . self::$action;
    }

    /**
     * 获取rewrite路径
     * @param $s
     * @return string
     */
    protected static function getRewriteUrl($s): string
    {
        $s = ltrim(str_replace(array('s=/', HTML_SUFFIX), array('', ''), $s), '/');
        $position = strpos($s, '&');
        if ($position !== false) {
            $s = substr($s, 0, $position);
        }
        return $s;
    }

    /**
     * 获取pathinfo路径
     * @param $s
     * @return string
     * @throws Exception
     */
    protected static function getPathInfoUrl($s): string
    {
        parse_str($s, $u);
        $m = $u['m'] ?? Config::get('app.default_model');
        $c = $u['c'] ?? Config::get('app.default_controller');
        $a = $u['a'] ?? Config::get('app.default_action');
        return $m . '/' . $c . '/' . $a;
    }


    /**
     * 根据请求获取URL路径
     * @param $s
     * @return string
     * @throws \yogurt\Exception
     */
    public static function getUrl($s): string
    {
        if (empty($s) || $s == '/') {
            return self::getUrlByConfig();
        }
        $urlModel = Config::get('app.url_model');
        $u = '';
        switch ($urlModel) {
            case 0:
                if (strpos($s, '/') !== false) {
                    $u = self::getRewriteUrl($s);
                } else {
                    $u = self::getPathInfoUrl($s);
                }
                break;
            case 1:
                $u = self::getPathInfoUrl($s);
                break;
            case 2:
                $u = self::getRewriteUrl($s);
                break;
            default:
                break;
        }
        self::setUrlArr($u);
        return self::$module . '/' . self::$controller . '/' . self::$action;
    }

    /**
     * url转数组
     * @param $u
     * @throws \yogurt\Exception
     */
    public static function setUrlArr($u)
    {
        self::setUrl($u);
    }

    /**
     * 设置url
     * @param $route
     * @throws \yogurt\Exception
     */
    public static function setUrl($route)
    {
        $urlArr = explode('/', $route);
        if (!empty($urlArr)) {
            self::$module = $urlArr[0] ?? Config::get('app.default_model');
            self::$controller = $urlArr[1] ?? Config::get('app.default_controller');
            self::$action = $urlArr[2] ?? Config::get('app.default_action');
        }
    }

    /**
     * 根据URL获取真实类文件
     * @param $r
     * @return string
     * @throws Exception
     */
    protected static function getRealClassFile($r): string
    {
        $rs = explode('/', $r);
        $end = array_pop($rs);
        array_push($rs, Config::get('app.controller_directory'), $end);
        $s = join('/', $rs);
        return $s . PHP_SUFFIX;
    }

    /**
     * 获取类文件，不含后缀
     * @param $classNameFile
     * @return array|string|string[]
     */
    public static function getClass($classNameFile)
    {
        return str_replace(array(PHP_SUFFIX, '/'), array('', '\\'), $classNameFile);
    }


}
