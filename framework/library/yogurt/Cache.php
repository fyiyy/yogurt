<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

/**
 * Class Cache
 * @package yogurt
 * @method \yogurt\cache\Builder set(string $key, mixed $value, int $expire) static 设置缓存
 * @method \yogurt\cache\Builder get(string $key) static 获取指定缓存
 * @method \yogurt\cache\Builder rm(string $key) static 删除指定缓存
 * @method \yogurt\cache\Builder clear() static 清空所有缓存
 */
class Cache
{

    public function __construct()
    {
    }

    /**
     * 注册缓存驱动类
     * @param ...$args
     * @return mixed
     * @throws Exception
     */
    private static function register(...$args)
    {
        return \yogurt\cache\Builder::bind($args);
    }

    /**
     * 缓存类入口
     * @param $method
     * @param $args
     * @return mixed
     * @throws \yogurt\Exception
     */
    public static function __callStatic($method, $args)
    {
        return static::register($method, $args);
    }

    /**
     * 容器调用入口
     * @param $method
     * @param $args
     * @return mixed
     * @throws \yogurt\Exception
     */
    public function __call($method, $args)
    {
        return static::register($method, $args);
    }

    private function __clone()
    {
    }
}
