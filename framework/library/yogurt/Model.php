<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------
namespace yogurt;

/**
 * Class Model
 * @package yogurt
 * @method \yogurt\db\Builder where(mixed $field, string $op = null, mixed $condition = null) static 查询条件
 * @method \yogurt\db\Builder insert(array $data) static 添加数据
 * @method \yogurt\db\Builder update(array $data) static 更新数据
 * @method \yogurt\db\Builder insertAll(array $data) static 批量添加数据
 * @method \yogurt\db\Builder insertGetId(array $data) static 添加数据并返回主键值
 * @method \yogurt\db\Builder select() static 查询多条数据函数
 * @method \yogurt\db\Builder find($primary) static 查询单条数据函数
 * @method \yogurt\db\Builder count() static 查询统计数据
 * @method \yogurt\db\Builder delete($model = false) static 删除数据
 * @method \yogurt\db\Builder union(string $option) static 合并查询
 * @method \yogurt\db\Builder field(string $field) 查询指定字段数据
 * @method \yogurt\db\Builder order(string $option) 排序查询
 * @method \yogurt\db\Builder limit(int $page, int $pageSize) 分页查询
 * @method \yogurt\db\Builder lock(string $option) 查询锁定数据库
 * @method \yogurt\db\Builder comment(string $option) 添加sql注释
 * @method \yogurt\db\Builder distinct(bool $option) 数据去重
 * @method \yogurt\db\Builder group(string $option) 数据分组查询
 * @method \yogurt\db\Builder force(string $option) 强制使用索引
 * @method \yogurt\db\Builder fetchSql() 打印sql
 * @method \yogurt\db\Builder value(string $field) 获取指定字段值
 * @method \yogurt\db\Builder alias(string $alias) 数据库表别名
 * @method \yogurt\db\Builder cache(int $expire) 缓存查询
 * @method \yogurt\db\Builder join(string $option, string $where, $model = 'left') 数据库表连接
 */
class Model implements \JsonSerializable, \ArrayAccess
{

    protected $tableName = null;


    public static function __callStatic($method, $args)
    {
        $model = static::class;
        $tableName = str_replace('\\', '/', $model);
        $tableName = strtolower(basename($tableName));

        $model = new $model();
        $getName = $model->getName();
        if (!empty($getName)) {
            $tableName = $getName;
        }

        return call_user_func_array([empty(Config::get('database.prefix')) ? DB::table($tableName) : DB::name($tableName), $method], $args);
    }


    public function getName()
    {
        return $this->tableName;
    }

    public function __get($name)
    {
        return $this->$name;
    }

    public function __set($name, $value)
    {
        $this->$name = $value;
    }

    // ArrayAccess
    public function offsetExists($offset)
    {
        return property_exists(static::class, $offset);
    }

    public function offsetGet($offset)
    {
        return $this->$offset;
    }

    public function offsetSet($offset, $value)
    {
        $this->$offset = $value;
    }

    public function offsetUnset($offset)
    {
        $this->$offset = '';
    }

    // JsonSerializable
    public function jsonSerialize()
    {
        // TODO
    }
}


?>