<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------
namespace yogurt;

/**
 * Class Session，框架Session类不再支持$_SESSION操作
 * @package yogurt\session
 * @method \yogurt\session\Builder get(string $key) static 获取Session数据
 * @method \yogurt\session\Builder set(string $key, mixed $option, int $expire = 0) static 设置Session数据
 * @method \yogurt\session\Builder init(array $config) static 配置Session数据
 * @method \yogurt\session\Builder delete(string $key) static 删除Session数据
 * @method \yogurt\session\Builder clear() static 清空Session数据
 */
class Session
{

    private static $session;

    private static function start()
    {
        if (is_null(self::$session)) {
            $driver = ucfirst(Config::get('session.driver'));
            $class = 'yogurt\\session\\driver\\' . $driver . 'Handler';
            self::$session = new $class();
        }
        return self::$session;
    }

    public function __call($method, $params)
    {
        return call_user_func_array([self::start(), $method], $params);
    }

    public static function __callStatic($name, $arguments)
    {
        return call_user_func_array([static::start(), $name], $arguments);
    }


}
