<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt\cache\driver;


use yogurt\cache\Builder;

class File extends Builder
{


    /**
     * 设置缓存，过期时间为秒
     * @param mixed ...$args
     */
    public static function set(...$args)
    {
        $key = $args[0][0];
        $value = $args[0][1];
        $expire = $args[0][2] ?? 0;
        $file = self::$_cachePath . md5(self::$_prefix . $key) . PHP_SUFFIX;
        $data = [
            'value' => $value,
            'expire' => $expire
        ];
        $data = json_encode($data, JSON_UNESCAPED_UNICODE);
        return file_put_contents($file, $data);
    }

    /**
     * 获取缓存
     * @param $key
     * @return false|mixed
     */
    public static function get($key)
    {
        $file = self::$_cachePath . md5(self::$_prefix . $key[0]) . PHP_SUFFIX;
        if (!file_exists($file)) return false;
        $data = json_decode(file_get_contents($file), true);
        if ($data['expire'] != 0 && (time() - filemtime($file) > $data['expire'])) {
            // 删除过期文件
            unlink($file);
            return false;
        }
        return $data['value'];
    }

    /**
     * 删除指定缓存
     * @param $key
     * @return bool
     */
    public static function rm($key): bool
    {
        $file = self::$_cachePath . md5(self::$_prefix . $key[0]) . PHP_SUFFIX;
        if (!file_exists($file)) return false;
        // 删除过期文件
        unlink($file);
        return true;
    }

    /**
     * 清空所有缓存
     * @return bool
     */
    public static function clear(): bool
    {
        $deleteFile = array_map('unlink', glob(self::$_cachePath . '*'));
        return empty($deleteFile) ? false : $deleteFile[0];
    }
}