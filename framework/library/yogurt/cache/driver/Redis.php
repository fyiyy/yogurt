<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt\cache\driver;


use yogurt\cache\Builder;
use yogurt\Config;
use yogurt\Exception;

class Redis extends Builder
{

    private static $_redis;

    /**
     * 设置缓存，过期时间为秒
     * @param mixed ...$args
     */
    private static function set(...$args)
    {
        return self::$_redis->set(md5(self::$_prefix . $args[0][0]), json_encode($args[0][1]), $args[0][2]);
    }

    /**
     * 获取缓存
     * @param ...$args
     * @return mixed
     */
    private static function get(...$args)
    {
        return json_decode(self::$_redis->get(md5(self::$_prefix . $args[0][0])), JSON_UNESCAPED_UNICODE);
    }

    /**
     * 删除指定缓存
     * @param $key
     * @return mixed
     */
    private static function rm($key)
    {
        return self::$_redis->del(md5(self::$_prefix . $key[0]));
    }

    /**
     * 清空所有缓存
     * @return mixed
     */
    private static function clear()
    {
        return self::$_redis->flushAll();
    }

    /**
     * redis初始化连接
     * @throws \RedisException
     * @throws \yogurt\Exception
     */
    private static function getRedis()
    {
        if (is_null(self::$_redis)) {
            $redis = new \Redis();
            $config = Config::get('cache.redis');
            $redis->connect($config['host'], $config['port']);
            if (!empty($config['password'])) {
                $redis->auth($config['password']);
            }
            $pong = $redis->ping();
            if ($pong) {
                self::$_redis = $redis;
            }
        }
    }

    /**
     * 启动入口
     * @throws \RedisException
     * @throws \yogurt\Exception
     */
    public static function __callStatic($method, $args)
    {
        if (!method_exists(__CLASS__, $method)) {
            throw new Exception("class Redis has not " . $method . " method.\r\n");
        }
        self::getRedis();
        if (is_null(self::$_redis)) {
            throw new Exception("Redis connection failed.\r\n");
        }
        return static::$method($args[0]);
    }

}
