<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt\cache;


use yogurt\Config;
use yogurt\file\File;

abstract class Builder
{

    /**
     * 文件缓存路径
     * @var $_cachePath
     */
    protected static $_cachePath;

    /**
     * 缓存前缀
     * @var $_prefix
     */
    protected static $_prefix;

    /**
     * 缓存驱动绑定
     * @param $args
     * @return mixed
     * @throws \yogurt\Exception
     */
    public static function bind($args)
    {
        $type = empty(Config::get('cache.type')) ? 'file' : Config::get('cache.type');
        $className = 'yogurt\\cache\\driver\\' . ucwords($type);
        $method = $args[0];
        self::setConfig();
        return $className::$method($args[1]);
    }


    /**
     * 设置缓存配置
     * @throws \yogurt\Exception
     */
    protected static function setConfig()
    {
        self::$_cachePath = Config::get('cache.path');
        if (!is_dir(self::$_cachePath)) {
            File::directory(self::$_cachePath);
        }
        self::$_prefix = Config::get('cache.prefix');
    }


}
