<?php

namespace yogurt;

/**
 * Class Db
 * @package yogurt
 * @method \yogurt\db\Builder table(string $table) static 指定数据表（含前缀）
 * @method \yogurt\db\Builder name(string $name) static 指定数据表（不含前缀）
 * @method \yogurt\db\Builder startTrans() static 开启数据库事务
 * @method \yogurt\db\Builder commit() static 提交数据库事务
 * @method \yogurt\db\Builder rollback() static 回滚数据库事务
 * @method \yogurt\db\Builder query(string $sql) static 查询sql函数
 * @method \yogurt\db\Builder exec(string $sql) static 执行sql函数
 * @method \yogurt\db\Builder getTableInfo(string $table) static 获取表结构信息
 * @method \yogurt\db\Builder getPrimaryKey(string $table) static 获取表主键信息
 */
class Db
{

    private static $instance = [];


    /**
     * 数据库连接
     * @param string $option
     * @param string $name
     * @return mixed
     * @throws Exception
     */
    public static function connect($option = '', $name = '')
    {
        $config = self::getConfig($option);

        if (empty($name)) {
            $name = md5(serialize($config));
        }

        if (!isset(self::$instance[$name])) {
            $className = 'yogurt\\db\\driver\\' . ucwords($config['type']);
            self::$instance[$name] = new $className($config);
        }

        return self::$instance[$name];

    }

    /**
     * 获取配置数据库信息
     * @param string $option
     * @return array|mixed
     * @throws Exception
     */
    private static function getConfig($option)
    {
        $databaseConfig = Config::get('database');
        if (empty($option)) {
            return $databaseConfig['default'];
        }
        return $databaseConfig[$option];
    }

    /**
     * 静态类调用入口
     * @param $method
     * @param $args
     * @return mixed
     * @throws Exception
     */
    public static function __callStatic($method, $args)
    {
        return call_user_func_array([self::connect(), $method], $args);
    }


    /**
     * 非静态调用（兼容容器调用）
     * @param $method
     * @param $args
     * @return mixed
     * @throws Exception
     */
    public function __call($method, $args)
    {
        return call_user_func_array([self::connect(), $method], $args);
    }


}
