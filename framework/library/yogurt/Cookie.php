<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------
namespace yogurt;

class Cookie
{
    /**
     * 设置Cookie
     * @param $key
     * @param $value
     * @param int $expire
     * @return bool
     */
    public static function set($key, $value, $expire = 0): bool
    {
        if ($expire == 0) {
            setcookie($key, $value);
        } else {
            setcookie($key, $value, time() + $expire);
        }
        return true;
    }

    /**
     * 获取Cookie
     * @param $key
     * @return false|mixed
     */
    public static function get($key)
    {
        return $_COOKIE[$key] ?? false;
    }

    /**
     * 删除Cookie
     * @param $key
     * @return bool
     */
    public static function delete($key): bool
    {
        if (isset($_COOKIE[$key])) {
            setcookie($key, null, time() - 3600);
            unset($_COOKIE[$key]);
        }
        return true;
    }

    /**
     * 清除所有cookie
     */
    public static function clear()
    {
        if (!empty($_COOKIE)) {
            foreach ($_COOKIE as $key => $value) {
                if ($key != 'PHPSESSID') {
                    setcookie($key, null, time() - 3600);
                    unset($_COOKIE[$key]);
                }
            }
        }
    }

}
