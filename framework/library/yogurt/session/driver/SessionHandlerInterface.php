<?php

namespace yogurt\session\driver;

interface SessionHandlerInterface {
    public function open();
    public function read();
    public function gc();
    public function flush();
}