<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------
namespace yogurt;

class Exception extends \Exception
{

    //重定义构造器使第一个参数 message 变为必须被指定的属性
    public function __construct($message, $code = 0)
    {
        //可以在这里定义一些自己的代码
        //建议同时调用 parent::construct()来检查所有的变量是否已被赋值
        parent::__construct($message, $code);
    }

    public function __toString()
    {
    }

    /**
     * 自定义错误注册
     */
    public static function register($errorInfo = [])
    {
        if (!Config::get('app.debug') && !empty($errorInfo)) {
            echo "<script>document.title='系统发生错误';</script>";
            exit('<h3 style="text-align: center;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%);">系统错误~~~</h3>');
        }
        if (!empty($errorInfo)) {
            self::errorHandler($errorInfo);
        }
        // error错误注册
        self::exceptionHandler();
    }


    /**
     * 模板错误异常handle
     */
    public static function errorHandler($errorInfo)
    {
        self::showError($errorInfo);
    }

    /**
     * 语法错误异常handle
     */
    public static function exceptionHandler()
    {
        $lastError = error_get_last();
        self::showError($lastError);
    }

    /**
     * 读取文件$start到$end行内容
     * @param $file
     * @param $start
     * @param $end
     * @param int $length
     * @return array
     */
    public static function getLineContent($file, $start, $end, $length = 40960)
    {
        $i = 1; // 行数
        $handle = fopen($file, "r");
        $data = [];
        while (!feof($handle)) {
            $buffer = fgets($handle, $length);
            if ($i >= $start && $i <= $end) {
                $data[] = $buffer;
            }
            if ($i > $end) {
                break;
            }
            $i++;
        }
        fclose($handle);
        return $data;
    }

    /**
     * 显示错误信息
     * @param $errorInfo
     * @return void
     */
    public static function showError($errorInfo)
    {
        if (empty($errorInfo)) {
            return;
        }
        echo "<script>document.title='系统发生错误';</script>";
        $data = self::getLineContent($errorInfo['file'], $errorInfo['line'] - 5, $errorInfo['line'] + 5, 40960);
        foreach ($data as $key => $value) {
            if ($key == 5) {
                $data[$key] = "<p style='background-color: #f8cbcb;font-size: 14px;font-weight: bold;'>" . $value . "</p>";
            }
        }
        echo "<div style='display:inline-block;width: 90%;min-height: 80%;margin: 0 auto;position: absolute;left: 50%;top: 50%;transform: translate(-50%,-50%);border: 1px solid #ddd;padding: 20px;background: #f9f9f9;'>";
        echo "<h3><span style='text-decoration: underline;color: blue;'>Exception in " . $errorInfo['file'] . " line " . $errorInfo['line'] . "</span></h3>";
        echo "<h4><span>Message： " . $errorInfo['message'] . "</span></h4>";
        echo "<pre>" . implode(PHP_EOL, $data) . "</pre>";
        echo "</div>";
    }


}
