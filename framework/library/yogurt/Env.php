<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

/**
 * 环境变量类
 * Class Env
 * @package yogurt
 */
class Env
{
    /**
     * 环境变量前缀
     */
    const ENV_PREFIX = 'Y_';

    /**
     * 标记是否已经设置
     */
    private static $seted = false;

    /**
     * 环境变量文件
     * @var string $envFile
     */
    private static $envFile = ROOT_PATH . '.env';

    /**
     * 设置环境变量
     * @param void
     * @return void
     */
    private static function setEnv()
    {
        self::$seted = true;
        if (file_exists(self::$envFile)) {
            // 解析环境变量配置文件
            $env = parse_ini_file(self::$envFile, true);
            foreach ($env as $key => $val) {
                $prefix = static::ENV_PREFIX . strtoupper($key);
                if (is_array($val)) {
                    foreach ($val as $k => $v) {
                        $item = $prefix . '_' . strtoupper($k);
                        putenv("$item=$v");
                    }
                } else {
                    putenv("$prefix=$val");
                }
            }
        }
    }

    /**
     * @access public
     * @param string $name 环境变量名（支持二级 . 号分割）
     * @param bool $default 默认值
     * @return array|bool|string
     */
    public static function get(string $name, $default = false)
    {
        if (!self::$seted) {
            self::setEnv();
        }
        // 获取环境变量的值
        $result = getenv(static::ENV_PREFIX . strtoupper(str_replace('.', '_', $name)));
        if (false !== $result) {
            if ('false' === $result) {
                $result = false;
            } elseif ('true' === $result) {
                $result = true;
            }
            return $result;
        }
        return $default;
    }
}