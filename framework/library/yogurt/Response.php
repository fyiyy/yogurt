<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

class Response
{
    private static $_instance;

    /**
     * 请求相应
     * @param $array
     * @return false|string
     */
    public static function json($array, $die = false)
    {
        $data = json_encode($array, JSON_UNESCAPED_UNICODE);
        return $die ? exit($data) : $data;
    }

    /**
     * 重定向
     * @param $url
     */
    public static function redirect($url)
    {
        header('Location:' . $url);
    }

    public static function register()
    {
        if (!self::$_instance instanceof self) {
            self::$_instance = new self();
        }
        return self::$_instance;
    }

}

