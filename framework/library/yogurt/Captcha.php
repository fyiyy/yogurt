<?php
// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

class Captcha
{
    /**
     * 随机值
     * @var string
     */
    protected $data = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789';

    /**
     * 验证码保存session的值
     * @var string
     */
    protected $key = '';

    /**
     * 配置项
     * @var array
     */
    protected $config = [
        // 验证码字体大小
        'fontSize' => 20,
        // 验证码位数
        'length' => 4,
        // 验证码宽度
        'width' => 100,
        // 验证码高度
        'height' => 30,
        // 验证码有效期
        'expire' => 1800,
        // 关闭验证码杂点
        'useNoise' => false,
    ];

    public function __construct($config = [])
    {
        $this->config = array_merge($this->config, $config);
        $this->key = md5('yogurt_captcha');

    }

    /**
     * 生成验证码
     * @return bool
     */
    public function entry(): bool
    {
        // 创建黑色画布
        $image = imagecreatetruecolor($this->config['width'], $this->config['height']);
        // 为画布定义(背景)颜色
        $bgcolor = imagecolorallocate($image, 255, 255, 255);
        // 填充颜色
        imagefill($image, 0, 0, $bgcolor);
        // 创建一个变量存储产生的验证码数据
        $captcha = "";
        for ($i = 0; $i < $this->config['length']; $i++) {
            // 字体颜色
            $fontcolor = imagecolorallocate($image, mt_rand(0, 120), mt_rand(0, 120), mt_rand(0, 120));
            // 设置字体内容
            $fontcontent = substr($this->data, mt_rand(0, strlen($this->data) - 1), 1);
            $captcha .= $fontcontent;
            // 显示的坐标
            $x = ($i * $this->config['width'] / $this->config['length']) + $this->config['length'];
            $y = mt_rand(5, 10);
            // 填充内容到画布中
            imagestring($image, $this->config['fontSize'], $x, $y, $fontcontent, $fontcolor);
        }

        // 设置背景干扰元素
        if ($this->config['useNoise'] == false) {
            for ($$i = 0; $i < $this->config['width'] * 2; $i++) {
                $pointcolor = imagecolorallocate($image, mt_rand(50, 200), mt_rand(50, 200), mt_rand(50, 200));
                imagesetpixel($image, mt_rand(1, $this->config['width']), mt_rand(1, 29), $pointcolor);
            }
            //  设置干扰线
            for ($i = 0; $i < $this->config['length']; $i++) {
                $linecolor = imagecolorallocate($image, mt_rand(50, 200), mt_rand(50, 200), mt_rand(50, 200));
                imageline($image, mt_rand(1, $this->config['width']), mt_rand(1, 29), mt_rand(1, 99), mt_rand(1, 29), $linecolor);
            }
        }
        // 图片格式输出
        header('Content-Type: image/png');
        if (imagepng($image) && imagedestroy($image)) {
            // 存储在session里
            Session::set($this->key, $captcha, $this->config['expire']);
            return true;
        }
        return false;
    }


    /**
     * 检测验证码值
     * @param $value
     * @return bool
     */
    public function check($value): bool
    {
        if (strtolower($value) == strtolower(Session::get($this->key)) && !empty(Session::get($this->key))) {
            return true;
        }
        return false;
    }


}
