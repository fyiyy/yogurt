<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

/**
 * Class Controller
 * @package yogurt
 */
class Controller
{

    /**
     * @throws \ReflectionException
     */
    public function __get($name)
    {
        $className = __NAMESPACE__ . '\\' . ucwords($name);
        return app()->make($className);
    }

    /**
     * 分配变量到html模板
     * @param $key
     * @param $value
     */
    public function assign($key, $value)
    {
        View::assign($key, $value);
    }

    /**
     * 加载html模板
     * @param string $html
     */
    public function display($html = ''): string
    {
        return View::display($html);
    }

    /**
     * 加载html模板
     * @param string $html
     */
    public function fetch($html = ''): string
    {
        return $this->display($html);
    }

    /**
     * 当前请求的模块
     * @return mixed
     */
    public function model(): string
    {
        return Builder::$module;
    }

    /**
     * 当前请求的控制器
     * @return mixed
     */
    public function controller(): string
    {
        return Builder::$controller;
    }

    /**
     * 当前请求的方法
     * @return mixed
     */
    public function action(): string
    {
        return Builder::$action;
    }

    /**
     * 成功响应函数
     * @param $message
     * @param $url
     */
    public function success($message, $url)
    {
    }

    /**
     * 失败响应函数
     * @param $message
     * @param $url
     */
    public function error($message, $url)
    {
    }
}
