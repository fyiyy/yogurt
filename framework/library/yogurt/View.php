<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;

use yogurt\file\File;
use yogurt\template\Compile;

/**
 * Class View
 * @package yogurt
 */
class View
{
    /**
     * 编译后存放的目录
     * @var string $compileDir
     */
    private static $compileDir = TEMP_PATH;

    /**
     * 模板所在的文件夹
     * @var string $templateDir
     */
    private static $templateDir = '';

    /**
     * 模板的后缀
     * @var string $suffix
     */
    private static $suffix = HTML_SUFFIX;

    /**
     * 模板文件名,不带路径
     * @var $file
     */

    public static $file;
    /**
     * 值栈
     * @var array $value
     */

    private static $value = array();

    /**
     * 编译器
     * @var $compileTool
     */
    private static $compileTool;


    /**
     * 获取当前模块的视图文件夹
     * @return string
     * @throws Exception
     */
    public static function getViewPath(): string
    {
        return Builder::$module . DIRECTORY_SEPARATOR . Config::get('app.view_directory') . DIRECTORY_SEPARATOR;
    }


    /**
     * 注入变量到模板，支持单个变量和数组
     * @throws Exception
     */
    public static function assign()
    {
        $num = func_num_args();
        $args = func_get_args();
        if ($num == 0) {
            throw new Exception("function assign at least one parameter.\r\n");
        } elseif ($num == 1) {
            foreach ($args[0] as $k => $v) {
                self::$value[$k] = $v;
            }
        } elseif ($num == 2) {
            self::$value[$args[0]] = $args[1];
        }
    }

    /**
     * 获取模板的位置
     * @return string
     * @throws Exception
     */
    public static function path(): string
    {

        self::$templateDir = self::getViewPath();
        if (strpos(self::$file, DIRECTORY_SEPARATOR) !== false) {
            self::$templateDir .= self::$file;
        } else {
            self::$templateDir .= Builder::$controller . DIRECTORY_SEPARATOR . self::$file;
        }
        $file = APP_PATH . self::$templateDir . self::$suffix;
        if (!file_exists($file)) {
            throw new Exception("【{$file}】 template file not found.\r\n");
        }
        return $file;
    }


    /**
     * 渲染模板
     * @param string $file
     * @return string
     * @throws \yogurt\Exception
     */
    public static function display($file = ''): string
    {
        self::$file = empty($file) ? Request::controller() . DIRECTORY_SEPARATOR . Request::action() : $file;
        // 目录不存在则创建
        if (!is_dir(self::$compileDir)) {
            File::directory(self::$compileDir);
        }
        // 检测目录是否可写
        if (!is_writable(RUNTIME_PATH)) {
            throw new Exception("【" . RUNTIME_PATH . "】 is permission.\r\n");
        }
        $compileFile = self::$compileDir . md5(Request::model() . DIRECTORY_SEPARATOR . self::$file) . '.php';
        self::$compileTool = new Compile(self::path(), $compileFile);
        extract(self::$value, EXTR_OVERWRITE);
        if (!is_file($compileFile) || fileatime($compileFile) < filemtime(self::path())) {
            self::$compileTool->value = self::$value;
            self::$compileTool->compile();
        }
        ob_start();
        include $compileFile;
        $content = ob_get_clean();
        return empty($content) ? '' : $content;
    }

}