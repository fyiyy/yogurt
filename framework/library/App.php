<?php

// +----------------------------------------------------------------------
// | yogurt
// +----------------------------------------------------------------------
// +----------------------------------------------------------------------
// | Author: fengyi <1719847255.qq.com>
// +----------------------------------------------------------------------

namespace yogurt;


/**
 * 框架启动类
 */
class App
{
    /**
     * 自动加载框架核心类文件
     * @param $className
     */
    public static function autoload($className)
    {
        $prefix = __DIR__;
        if (strpos($className, __NAMESPACE__) === false) {
            $prefix = APP_PATH;
            if (strpos($className, 'app\\') === false) {
                $prefix = '';
            }
        }
        if (!empty($prefix)) {
            $className = $prefix . DIRECTORY_SEPARATOR . $className . PHP_SUFFIX;
            $className = str_replace(array('\\', 'app' . DIRECTORY_SEPARATOR, '//'), array(DIRECTORY_SEPARATOR, DIRECTORY_SEPARATOR, ''), $className);
            include $className;
        }
    }

    /**
     * 框架启动入口
     */
    public static function start()
    {
        spl_autoload_register([__CLASS__, 'autoload']);
        try {
            Builder::boot();
        } catch (\ReflectionException | Exception $e) {
        }
        spl_autoload_unregister([__CLASS__, 'autoload']);
    }
}
