<?php

use yogurt\Route;

Route::get('/hello', function () {
    return 'hello';
});

Route::get('/route', function () {
    return 'route';
});

Route::get('/index/index', '/index/index/index');
