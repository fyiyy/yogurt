<?php

namespace app\index\controller;

/**
 * Class Index
 * @package index\controller
 */
class Index
{

    /**
     * 首页方法
     */
    public function index(): string
    {
        return '<!DOCTYPE html><html lang="en"><head><meta charset="UTF-8"><title>yogurt是基于php的轻量级框架^_^</title></head><body><p style="font-size: 30px;text-align: center;position: absolute;top: 50%;left: 50%;transform: translate(-50%,-50%);">yogurt是基于php的轻量级框架^_^</p></body></html>';
    }

    public function hello($name): string
    {
        return 'hello ' . $name;
    }


}

