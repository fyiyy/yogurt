# yogurt

#### 介绍

yogurt是基于php的mvc框架

#### 软件架构

软件架构说明

#### 安装教程

1. 下载地址：https://gitee.com/fyiyy/yogurt.git

## 使用说明

#### 基础

1.安装yogurt

yogurt的环境要求如下：

> PHP >= 7.3
>
> PDO PHP Extension
>
> MBstring PHP Extension
>
> CURL PHP Extension


2.开发规范

3.目录结构

project 应用部署目录

> ├─application 应用目录（可设置）
>
> ├─config 公共配置
>
> ├─framework 框架系统目录
>
> ├─public                WEB 部署目录（对外访问目录）
>
> │ ├─static 静态资源存放目录(css,js,image)
>
> │ ├─index.php 应用入口文件
>
> │ ├─router.php 快速测试文件
>
> │ └─.htaccess 用于 apache 的重写
>
> ├─runtime 应用的运行时目录（可写，可设置）
>
> ├─vendor 第三方类库目录（Composer）

#### 架构

1.架构总览

2.入口文件

默认的应用入口文件位于public/index.php，内容如下：

````php
namespace yogurt;

require_once __DIR__ . '/../framework/start.php';

````

3.URL访问

没有启用路由的情况下典型的URL访问规则是：
> http://serverName/模块/控制器/操作?[参数名=参数值...]
> http://serverName/index.php（或者其它应用入口文件）?m=模块&c=控制器&a=操作&[参数名=参数值...]

#### 配置

1.配置目录

系统默认的配置文件目录就是项目目录（ROOT_PATH），也就是默认的project下面，并分为应用配置（整个应用有效）和模块配置（仅针对该模块有效）。

2.配置格式

返回PHP数组的方式是默认的配置定义格式，例如：

````php
return [
    // 调试模式
    'debug'                 => true,
    // 访问日志写入
    'log_write'             => true,
    // 默认模板后缀
    'default_template_exit' => 'html',
    // URL模式, 可选值:0=>兼容模式，1=>pathinfo，2=>rewrite
    'url_model'             => 0,
    // 默认模块
    'default_model'         => 'index',
    // 默认控制器
    'default_controller'    => 'index',
    // 默认方法
    'default_action'        => 'index',
    // 控制器文件夹,自定义修改必须保证有对应的目录
    'controller_directory'  => 'controller',
    // 视图文件夹，自定义修改必须保证有对应的目录
    'view_directory'        => 'view',
    // 程序app文件名
    'app_name'              => 'application',
    // 是否强制开启路由模式
    'url_route_must'        => false,
];
````

3.读取配置

设置完配置参数后，就可以使用get方法读取配置了，例如：

````php
echo Config::get('配置参数1');
````

如果需要读取二级配置，可以使用：

````php
echo Config::get('配置参数.二级参数');
````

4.环境变量配置

在开发过程中，可以在应用根目录下面的.env来模拟环境变量配置，.env文件中的配置参数定义格式采用ini方式，例如：

````php
app_debug =  true
app_trace =  true
````

````php
[DATABASE]
HOSTNAME = 127.0.0.1,127.0.0.1
DATABASE = yogurt
USERNAME =  root
PASSWORD =  root,root
````

#### 路由

1.路由模式

2.路由定义

在route目录下面定义任意php文件，目前路由可支持模糊匹配，暂不支持正则匹配

````php
use yogurt\Route;

Route::get('/hello', function () {
    return 'hello';
});

Route::post('/route', function () {
    return 'route';
});

Route::any('/index/hello', '/index/index/yogurt');
````

#### 控制器

1.控制器定义

yogurt的控制器定义比较灵活，可以无需继承任何的基础类，也可以继承官方封装的\yogurt\Controller类或者其他的控制器类。

###### 控制器定义

一个典型的控制器类定义如下：

````php
namespace app\index\controller;

class Index 
{
    public function index()
    {
        return 'index';
    }
}
````

控制器类文件的实际位置是：

````php
application\index\controller\Index.php
````

如果继承了yogurt\Controller类的话，可以直接调用yogurt\View及yogurt\Request类的方法，例如：

````php
namespace app\index\controller;

use yogurt\Controller;

class Index extends Controller
{
    public function index()
    {
        // 获取包含域名的完整URL地址
        $this->assign('domain',$this->request->url(true));
        return $this->fetch('index');
    }
}
````

2.控制器重定向

\yogurt\Controller类的redirect方法可以实现页面的重定向功能。

````php
//重定向到News模块的Category操作
Response::redirect('News/Category');
````

3.前置后置方法

````php
public function index()
{
    // 获取包含域名的完整URL地址
    $this->assign('domain',$this->request->url(true));
    return $this->fetch('index');
}
    
public function __beforeIndex()
{
    halt('index方法执行之前自动调用', false);
}

public function __afterIndex()
{
    halt('index方法执行之后自动调用', false);
}
````
#### 请求

1.请求信息

如果要获取当前的请求信息，可以使用\yogurt\Request类， 除了下文中的

````php
Request::get('id');
````

2.请求类型

````php
Request::isPost();
````

3.参数绑定

按名称绑定
参数绑定方式默认是按照变量名进行绑定，例如，我们给Blog控制器定义了两个操作方法read和archive方法，由于read操作需要指定一个id参数，archive方法需要指定年份（year）和月份（month）两个参数，那么我们可以如下定义：

````php
namespace app\index\Controller;

class Blog 
{
    public function read($id)
    {
        return 'id='.$id;
    }

    public function archive($year='2016',$month='01')
    {
        return 'year='.$year.'&month='.$month;
    }
}
````

URL的访问地址分别是：

````php
http://serverName/index.php/index/blog/read/?id=1
http://serverName/index.php/index/blog/archive?year=2016&month=06
````

4.依赖注入

Yogurt的依赖注入（也称之为控制反转）是一种较为轻量的实现，无需任何的配置，并且主要针对访问控制器进行依赖注入。可以在控制器的构造函数或者操作方法（指访问请求的方法）中类型声明任何（对象类型）依赖，这些依赖会被自动解析并注入到控制器实例或方法中。

在控制器的架构方法中会自动注入当前请求对象，例如：

````php
namespace app\index\controller;

use yogurt\Request;

class Index
{
	public function hello(Request $request)
    {
        return 'Hello,' . $request->param('name') . '！';
    }
    
}
````

#### 数据库

1.连接数据库

常用的配置在config目录下面的database.php中，默认使用default参数进行数据库连接，配置参数如下：

````php
use yogurt\Env;

return [
    'default' => [
        // 数据库类型：mysql,sqlsrv
        'type'               => 'mysql',
        // 服务器地址
        'hostname'           => Env::get('database.hostname', '127.0.0.1'),
        // 数据库名
        'database'           => Env::get('database.database', 'db1'),
        // 用户名
        'username'           => Env::get('database.username', 'root'),
        // 密码
        'password'           => Env::get('database.password', 'root'),
        // 端口
        'hostport'           => '3306',
        // 连接dsn
        'dsn'                => '',
        // 数据库连接参数
        'params'             => [],
        // 数据库编码默认采用utf8
        'charset'            => 'utf8',
        // 数据库表前缀
        'prefix'             => '',
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'             => 0,
        // 数据库读写是否分离 主从式有效
        'rw_separate'        => true,
        // 读写分离后 主服务器数量
        'master_number'      => 1
    ],
    'mydatabase' => [
        // 数据库类型：mysql,sqlsrv
        'type'               => 'mysql',
        // 服务器地址
        'hostname'           => Env::get('database.hostname', '192.168.2.8,127.0.0.1,192.168.2.10'),
        // 数据库名
        'database'           => Env::get('database.database', 'my_test'),
        // 用户名
        'username'           => Env::get('database.username', 'root'),
        // 密码
        'password'           => Env::get('database.password', 'root'),
        // 端口
        'hostport'           => '3306',
        // 连接dsn
        'dsn'                => '',
        // 数据库连接参数
        'params'             => [],
        // 数据库编码默认采用utf8
        'charset'            => 'utf8',
        // 数据库表前缀
        'prefix'             => '',
        // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
        'deploy'             => 1,
        // 数据库读写是否分离 主从式有效
        'rw_separate'        => true,
        // 读写分离后 主服务器数量
        'master_number'      => 1
    ],
    // 更多数据库连接
];
````

2.基本使用

配置了数据库连接信息后，我们就可以直接使用数据库运行原生SQL操作了，支持query（查询操作）和execute（写入操作）方法，并且支持参数绑定。

````php
Db::query("select * from y_user where id=1");
Db::execute("insert into y_user (id, name) values (1, 'yogurt')");
````

自定义数据库连接
````php
$user1 = Db::connect('mydatabase')->table('user')->order('id DESC')->limit(1,2)->select();

Db::connect('mydatabase')->startTrans();
$insertGetId = Db::connect('mydatabase')->table('user')->insertGetId(['name' => 'yogurt', 'password' => '123456']);
Db::connect('mydatabase')->commit();
````
3.查询构造器

查询一个数据使用：

````php
// table方法必须指定完整的数据表名
Db::table('y_user')->where('id',1)->find();
````

查询数据集使用：

````php
Db::table('y_user')->where('status',1)->select();

````

###### 添加数据

使用 Db 类的 insert 方法向数据库提交数据

````php
$data = ['name' => 'yogurt'];
Db::table('y_user')->insert($data);
````

###### 添加多条数据

````php
$data = [
    ['name' => 'yogurt'],
    ['name' => 'yogurt1'],
    ['name' => 'yogurt2']
];
Db::name('user')->insertAll($data);
````

###### 更新数据

````php
Db::table('y_user')->where('id', 1)->update(['name' => 'yogurt']);
````

###### 删除数据表中的数据

````php
Db::table('y_user')->where('id',1)->delete();
````

###### 链式操作

表达式查询

````php
Db::table('y_user')
    ->where('id','>',1)
    ->where('name','yogurt')
    ->select(); 
````

数组条件

````php
$map['name'] = 'yogurt';
$map['status'] = 1;
// 把查询条件传入查询方法
Db::table('y_user')->where($map)->select(); 
````

字段自增

````php
Db::name('user')->where('id',1)->setInc('number', 1); 
````

字段自减

````php
Db::name('user')->where('id',1)->setDec('number', 1); 
````

4.分布式数据库

Yogurt内置了分布式数据库的支持，包括主从式数据库的读写分离，但是分布式数据库必须是相同的数据库类型。

配置database.deploy 为1 可以采用分布式数据库支持。如果采用分布式数据库，定义数据库配置信息的方式如下

````php
//分布式数据库配置定义
return [
    'mydatabase' => [
            // 数据库类型：mysql,sqlsrv
            'type'               => 'mysql',
            // 服务器地址
            'hostname'           => Env::get('database.hostname', '192.168.2.8,127.0.0.1,192.168.2.10'),
            // 数据库名
            'database'           => Env::get('database.database', 'my_test'),
            // 用户名
            'username'           => Env::get('database.username', 'root'),
            // 密码
            'password'           => Env::get('database.password', 'root'),
            // 端口
            'hostport'           => '3306',
            // 连接dsn
            'dsn'                => '',
            // 数据库连接参数
            'params'             => [],
            // 数据库编码默认采用utf8
            'charset'            => 'utf8',
            // 数据库表前缀
            'prefix'             => '',
            // 数据库部署方式:0 集中式(单一服务器),1 分布式(主从服务器)
            'deploy'             => 1,
            // 数据库读写是否分离 主从式有效
            'rw_separate'        => true,
            // 读写分离后 主服务器数量
            'master_number'      => 1
        ],
];
````

#### 模型

1.定义

#### 验证器

Yogurt验证使用独立的\yogurt\Validate类或者验证器进行验证。

1.定义

````php
<?php

namespace app\index\validate;

use yogurt\Validate;

class User extends Validate
{
    protected $rule = [
        'name' => 'required|length:10,20',
        'age' => '10',
        'height' => '180|min:170|max:180',
        'email' => 'required|email｜length:13',
        'total' => 'number|between:120,1',
    ];

    protected $message = [
        'name.required' => '用户名必须',
        'name.length' => '用户名长度必须为10到20个字符',
        'height.min' => '身高最小值为@:min',
        'height.max' => '身高最大值为@:max',
        'age.number' => '年龄必须为数字',
        'age' => '年龄必须等于10',
        'height' => '身高必须等于180',
        'email.required' => '邮箱不能为空',
        'email.email' => '邮箱格式错误',
        'total.number' => '总数必须为数字',
        'total.between' => '总数必须在1到120之间',
    ];

    // 自定义验证规则
    protected function checkName($key, $value, $rule)
    {
        return $rule == $value ? true : '名称错误';
    }
}

````

2.使用

````php

use app\index\validate\User;

$data = [
    'name'  => 'hello yogurt',
    'age'   => '10',
    'height'   => 180,
    'email' => 'yogurt@qq.com',
    'total' => 1,
];
$userValidate = new User();

halt($userValidate->check($data), false);
halt($userValidate->getError());

````

#### 视图

1.视图实例化

2.模版引擎

3.模版赋值

除了系统变量和配置参数输出无需赋值外，其他变量如果需要在模板中输出必须首先进行模板赋值操作，绑定数据到模板输出有下面几种方式：

````php
namespace index\app\controller;

class Index extends \yogurt\Controller
{
    public function index()
    {
        // 模板变量赋值
        $this->assign('name','Yogurt');
        $this->assign('email','1719847255@qq.com');
        // 或者批量赋值
        $this->assign([
            'name'  => 'Yogurt',
            'email' => '1719847255@qq.com'
        ]);
        // 模板输出
        return $this->fetch('index');
    }
}
````

4.模版渲染

#### 模版

1.模版定位

2.模版标签

普通标签用于变量输出和模板注释，普通模板标签默认以{ 和 } 作为开始和结束标识，并且在开始标记紧跟标签的定义，如果之间有空格或者换行则被视为非模板标签直接输出。 例如：{$name} 、{$vo['name']} 都属于正确的标签，而{
$name} 、{ $vo.name}则不属于。

要更改普通标签的起始标签和结束标签，可以更改下面的配置参数：

````php
return [
    // 模板后缀
    'view_suffix'   => 'html',
    // 标签左界定符
    'taglib_begin'  => '{',
    // 标签右界定符
    'taglib_end'    => '}',
    // 是否支持原生PHP标签
    'php_turn'     => true,
];
````

3.变量输出

4.模版注释

````php
{#...# 或者 *...#，注释}
````

5.模版继承

模板继承是一项更加灵活的模板布局方式，模板继承不同于模板布局，甚至来说，应该在模板布局的上层。模板继承其实并不难理解，就好比类的继承一样，模板也可以定义一个基础模板（或者是布局），并且其中定义相关的区块（block），然后继承（extend）该基础模板的子模板中就可以对基础模板中定义的区块进行重载。

因此，模板继承的优势其实是设计基础模板中的区块（block）和子模板中替换这些区块。

每个区块由{block} {/block}标签组成。 下面就是基础模板中的一个典型的区块设计（用于设计网站标题）：

````php
{block name="title"}<title>网站标题</title>{/block}
````

block标签必须指定name属性来标识当前区块的名称，这个标识在当前模板中应该是唯一的，block标签中可以包含任何模板内容，包括其他标签和变量，例如：

````php
{block name="title"}<title>{$web_title}</title>{/block}
````

你甚至还可以在区块中加载外部文件：

````php
{block name="include"}{include file="public/header" /}{/block}
````

一个模板中可以定义任意多个名称标识不重复的区块，例如下面定义了一个base.html基础模板：

````html

<html>
<head>
    <meta http-equiv="Content-Type" content="text/html; charset=utf-8">
    <title>{block name="title"}标题{/block}</title>
</head>
<body>
{block name="menu"}菜单{/block}
{block name="left"}左边分栏{/block}
{block name="main"}主内容{/block}
{block name="right"}右边分栏{/block}
{block name="footer"}底部{/block}
</body>
</html>

````

然后我们在子模板（其实是当前操作的入口模板）中使用继承：

````html
{extend name="base" /}
{block name="title"}{$title}{/block}
{block name="menu"}
<a href="/">首页</a>
<a href="/info/">资讯</a>
<a href="/bbs/">论坛</a>
{/block}
{block name="left"}{/block}
{block name="main"}
{volist name="$list" id="vo"}
<a href="/new/{$vo['id']}">{$vo['title']}</a><br/>
{$vo['content']}
{/volist}
{/block}
{block name="right"}
最新资讯：
{volist name="$news" id="new"}
<a href="/new/{$new['id']}">{$new['title']}</a><br/>
{/volist}
{/block}
{block name="footer"}
@Yogurt 版权所有
{/block}
````

6.包含文件

````html
{include file="public/header" /} // 包含头部模版header
````

7.内置标签

volist标签通常用于查询数据集（select方法）的结果输出，通常模型的select方法返回的结果是一个二维数组，可以直接使用volist标签进行输出。 在控制器中首先对模版赋值：

````php
$list = User::select();
$this->assign('list',$list);
````

在模版定义如下，循环输出用户的编号和姓名：

````html
{volist name="$list" id="vo"}
{$vo['id']}:{$vo['name']}<br/>
{/volist}
````

循环标签

````html

{foreach $data  item="$item"}
<p>{$item['id']} {$item['name']}</p>
{/foreach}

{foreach $data  as $key=>$vo}
<p>{$vo['id']} {$vo['name']}</p>
{/foreach}

````

IF标签

````html
{loop name="$data" id='vo'}
{if ($vo['id'] == 15)}
<p style="color: red;">{$vo['id']} {$vo['name']}</p>
{else}
<p>{$vo['id']} {$vo['name']}</p>
{/if}
{/loop}
````

#### 日志

1.日志写入

2.日志读取

#### 杂项

1.缓存

Yogurt采用yogurt\Cache类提供缓存功能支持。

缓存支持采用驱动方式，所以缓存在使用之前，需要进行连接操作，也就是缓存初始化操作。

###### 设置

````php
return [
    // 缓存类型为File，支持file,redis
    'type'  =>  'file',
    // 全局缓存有效期（0为永久有效）
    'expire'=>  0,
    // 缓存前缀
    'prefix'=>  'yogurt',
    // 缓存目录
    'path'  =>  '../runtime/cache/',
    // redis缓存配置
    'redis' => [
        'host'	   => '127.0.0.1',
        'port'     => 6379,
        'password' => ''
    ]
];
````

###### 使用

设置缓存（有效期一个小时）

````php
Cache::set('name',$value,3600);
````

获取缓存

````php
Cache::get('name');
````

删除缓存

````php
Cache::rm('name');
````

清空缓存

````php
Cache::clear();
````

2.Session

设置（有效期一个小时）

#### session配置文件位于config/session.php

````php
Session::init(['expire'=>20]);
Session::set('name',$value);
````

获取

````php
Session::get('name');
````

删除

````php
Session::delete('name');
````

清空

````php
Session::clear();
````

3.Cookie

4.上传

前端页面

````html
<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <title>Title</title>
</head>
<body>
<form action="upload.html" method="post" enctype="multipart/form-data">
    <input type="file" name="image" id="">
    <input type="submit" value="上传">
</form>
</body>
</html>
````

````php
public function upload()
{

    $file = Request::file('image')->validate(['size' => 1024, 'ext' => 'jpg,jpeg,png,gif,xls'])->rule('md5')->move('upload/');
    echo '<pre>';
    print_r($file->getError());
    echo '</pre>';
    echo '<pre>';
    print_r($file->getSaveName());
    echo '</pre>';
    exit();
}
````

#### 中间件

1.定义

在应用目录下面middleware目录中定义控制器中间件

````php
namespace app\middleware;


use Closure;

class CheckLogin
{

    /**
     * 响应处理请求
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        // 请求响应前
        $response = $next($request);
        // 请求响应后
        return $response;
    }

}
````

2.使用

在控制器中定义中间件属性$middleware

````php
namespace app\index\controller;

use yogurt\Controller;

class Base extends Controller
{
    /**
     * 中间件属性
     * @var string[] $middleware
     */
    protected $middleware = [
        'app\middleware\CheckLogin',
    ];

}
````

#### 容器

1.使用

````php
if (!app()->make('yogurt\Cache')->get('login')) {
    app()->make('yogurt\Cache')->set('login', 'yogurt', 10);
}
halt(app()->make('yogurt\Cache')->get('login'));
````

#### 命令行

1.启动内置服务器

不带参数命令默认会在本机8000端口启动服务

````php
php yogurt run
````

如果启动成功，会输出下面信息，并显示web目录位置。

````php
Yogurt Development server is started On <http://127.0.0.1:8000/>
You can exit with `CTRL-C`
Document root is: /Users/yf/Sites/yogurt/public
````

然后你可以直接在浏览器里面访问

````php
http://127.0.0.1:8000/
````

带参数命令如下：

````php
php yogurt run 127.0.0.1:8080
````
##### 默认启动的服务只能通过以下方式访问：
#### http://127.0.0.1:8080/index.php?m=index&c=index&a=hello&name=yogurt
##### 如果想要隐藏index.php文件可以指定参数执行以下命令

````php
php yogurt run 127.0.0.1:8080 index.php
````
#### 访问的地址如下：
#### http://127.0.0.1:8080/index/index/hello?name=yogurt
2.查看版本

````php
php yogurt version
````

3.生成模块

生成一个test模块的指令如下：

````php
php yogurt build --module test
````

会自动生成test模块，自动生成的模块目录包含了controller、model和view目录

4.创建类文件

###### 快速生成控制器

执行下面的指令可以生成index模块的Blog控制器类库文件

````php
php yogurt make --controller index/Blog
````

默认生成的是一个空控制器，类文件如下：

````php
<?php

namespace app\index\controller;


use yogurt\Controller;

class Blog extends Controller
{


}

?>
````

###### 快速生成模型

和生成控制器类似，执行下面的指令可以生成index模块的Blog模型类库文件

````php
php yogurt make --model index/Blog
````

生成的模型类文件如下：

````php
<?php
namespace app\index\model;


use yogurt\Model;

class Blog extends Model
{


}

?>
````

###### 快速生成中间件

可以使用下面的指令生成一个中间件类

````php
php yogurt make --middleware Auth
````

会自动生成一个 app\middleware\Auth类文件

````php
<?php

namespace app\middleware;


use Closure;

class Auth
{


    /**
     * 响应处理请求
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response = $next($request);

        return $response;

    }

}

?>
````

如果创建指定模块下面的中间件可以使用下面的命令

````php
php yogurt make --middleware index/Auth
````

生成的文件在对应的模块下面的middleware目录下

````php
<?php

namespace app\index\middleware;


use Closure;

class Auth
{


    /**
     * 响应处理请求
     * @param $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        $response = $next($request);

        return $response;

    }

}

?>
````

###### 创建验证器类

可以使用一下命令

````php
php yogurt make --validate index/Blog
````

生成一个 app\index\validate\Blog 验证器类，然后添加自己的验证规则和错误信息。

````php
<?php

namespace app\index\validate;


use yogurt\Validate;

class Blog extends Validate
{


    protected $rule = [];

    protected $message = [];


}

?>
````

5.清除缓存文件

如果需要清除应用的缓存文件，可以使用下面的命令：

````php
php yogurt clear
````

会清除runtime目录（包括模板缓存、日志文件及其子目录）下面的所有的文件，但会保留runtime目录。

#### 扩展

1.验证码

定义：

````php
use yogurt\Captcha;
$config = [
    // 验证码字体大小
    'fontSize' => 8,
    // 验证码位数
    'length' => 5,
    // 验证码宽度
    'width' => 250,
    // 验证码高度
    'height' => 80,
    // 验证码有效期
    'expire' => 1800,
    // 关闭验证码杂点
    'useNoise' => false,
];

$captcha = new Captcha($config);
return $captcha->entry();
````

使用方法：

在模版内添加验证码的显示代码

````html
<img src="对应的验证码地址">
````

检测验证码

````php
// 检测输入的验证码是否正确，$value为用户输入的验证码字符串
$captcha = new Captcha();
if( !$captcha->check($value))
{
	// 验证失败
}
````


